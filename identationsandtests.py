import threading
import time

class OncePerMinute(threading.Thread):
    def run(self):
        while True:
            print("Here I am, doing something")
            time.sleep(0.5)

t = OncePerMinute(daemon=True)
t.start()
for x in range(10):
    print("Here I am, doing other stuff number {}".format(x))
    time.sleep(0.1)
t.join(timeout=0)