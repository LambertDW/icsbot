
# Use with caution. This will crete a large file on your system
# if you leave it running.

from itertools import combinations_with_replacement
a_z = "abcdefghijklmnopqrstuvwxyz_0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

with open("passwords.txt", "a") as file:
    for i in range(5, 15):
        for subset in combinations_with_replacement(a_z, i):
            file.write("".join(subset)+"\n")